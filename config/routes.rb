Rails.application.routes.draw do
  root to: 'list#new'
  post 'list/new'
  get 'list/new'
  get 'list/data'
  resources :students
  resources :courses
  resources :institutions
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
