require 'rails_helper'

RSpec.describe ListService do
  it '#data' do
    student = create(:student)
    list = ListService.new.data

    expect(student.institution.name).to eq(list.first.institution_name)
    expect(student.institution.grade).to eq(list.first.institution_grade)
    expect(student.course.name).to eq(list.first.course_name)
    expect(student.course.grade).to eq(list.first.course_grade)
    expect(student.name).to eq(list.first.student_name)
    expect(student.grade).to eq(list.first.student_grade)
  end
end
