require 'rails_helper'

RSpec.describe 'institutions/edit', type: :view do
  before(:each) do
    @institution = assign(:institution, create(:institution))
  end

  it 'renders the edit institution form' do
    render

    expect(rendered).to have_field 'institution[name]', with: @institution.name
    expect(rendered).to have_field 'institution[grade]', with: @institution.grade
  end
end
