require 'rails_helper'

RSpec.describe 'institutions/new', type: :view do
  before(:each) do
    assign(:institution, Institution.new)
  end

  it 'renders new institution form' do
    render

    expect(rendered).to have_field 'institution[name]'
    expect(rendered).to have_field 'institution[grade]'
  end
end
