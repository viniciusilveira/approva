require 'rails_helper'

RSpec.describe 'students/new', type: :view do
  before(:each) do
    assign(:student, Student.new)
  end

  it 'renders new student form' do
    render

    expect(rendered).to have_field 'student[name]'
    expect(rendered).to have_field 'student[grade]'
    expect(rendered).to have_select 'student[course_id]'
  end
end
