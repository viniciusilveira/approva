require 'rails_helper'

RSpec.describe 'students/edit', type: :view do
  before(:each) do
    @student = assign(:student, create(:student))
  end

  it 'renders the edit student form' do
    render

    expect(rendered).to have_field 'student[name]', with: @student.name
    expect(rendered).to have_field 'student[grade]', with: @student.grade
    expect(rendered).to have_select 'student[course_id]', with_selected: @student.course.name
  end
end
