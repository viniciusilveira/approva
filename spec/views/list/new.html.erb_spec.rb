require 'rails_helper'

RSpec.describe 'list/new.html.erb', type: :view do
  before(:each) do
    assign(:list, ListService.new.data)
    create(:student)
  end

  it 'renders new institution form' do
    render

    expect(rendered).to have_content Institution.human_attribute_name :name
    expect(rendered).to have_content Institution.human_attribute_name :grade
    expect(rendered).to have_content Course.human_attribute_name :name
    expect(rendered).to have_content Course.human_attribute_name :grade
    expect(rendered).to have_content Student.human_attribute_name :name
    expect(rendered).to have_content Student.human_attribute_name :grade
  end
end
