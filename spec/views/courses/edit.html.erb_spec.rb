require 'rails_helper'

RSpec.describe 'courses/edit', type: :view do
  before(:each) do
    @course = assign(:course, create(:course))
  end

  it 'renders the edit course form' do
    render

    expect(rendered).to have_field 'course[name]', with: @course.name
    expect(rendered).to have_field 'course[grade]', with: @course.grade
    expect(rendered).to have_select 'course[institution_id]', with_selected: @course.institution.name
  end
end
