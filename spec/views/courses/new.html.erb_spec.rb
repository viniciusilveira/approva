require 'rails_helper'

RSpec.describe 'courses/new', type: :view do
  before(:each) do
    assign(:course, Course.new)
  end

  it 'renders new course form' do
    render

    expect(rendered).to have_field 'course[name]'
    expect(rendered).to have_field 'course[grade]'
    expect(rendered).to have_select 'course[institution_id]'
  end
end
