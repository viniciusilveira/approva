require 'rails_helper'

RSpec.describe StudentsController, type: :controller do
  let(:course) { create(:course) }

  let(:valid_attributes) { attributes_for(:student, course_id: course.id) }

  let(:invalid_attributes) { attributes_for(:student, name: nil) }

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}
      expect(response).to be_success
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      student = create(:student)
      get :edit, params: { id: student.to_param }
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Student' do
        expect do
          post :create, params: { student: valid_attributes }
        end.to change(Student, :count).by(1)
      end

      it 'redirects to the  root path after create student' do
        post :create, params: { student: valid_attributes }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { student: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        attributes_for(:student)
      end

      it 'updates the requested student' do
        student = create(:student)
        put :update, params: { id: student.to_param, student: new_attributes }
        student.reload
        expect(student.name).to eq(new_attributes[:name])
        expect(student.grade.to_s).to eq(new_attributes[:grade])
      end

      it 'redirects to the student' do
        student = create(:student)
        put :update, params: { id: student.to_param, student: valid_attributes }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        student = Student.create! valid_attributes
        put :update, params: { id: student.to_param, student: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested student' do
      student = Student.create! valid_attributes
      expect do
        delete :destroy, params: { id: student.to_param }
      end.to change(Student, :count).by(-1)
    end

    it 'redirects to the students list' do
      student = Student.create! valid_attributes
      delete :destroy, params: { id: student.to_param }
      expect(response).to redirect_to(root_path)
    end
  end
end
