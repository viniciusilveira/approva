require 'rails_helper'

RSpec.describe CoursesController, type: :controller do
  let(:institution) { create(:institution) }

  let(:valid_attributes) { attributes_for(:course, institution_id: institution.id) }

  let(:invalid_attributes) { attributes_for(:course, name: nil) }

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}
      expect(response).to be_success
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      course = create(:course)
      get :edit, params: { id: course.to_param }
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Course' do
        expect do
          post :create, params: { course: valid_attributes }
        end.to change(Course, :count).by(1)
      end

      it 'redirects to the root path after create course' do
        post :create, params: { course: valid_attributes }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { course: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        attributes_for(:course)
      end

      it 'updates the requested course' do
        course = create(:course)
        put :update, params: { id: course.to_param, course: new_attributes }
        course.reload
        expect(course.name).to eq(new_attributes[:name])
        expect(course.grade.to_s).to eq(new_attributes[:grade])
      end

      it 'redirects to the course' do
        course = create(:course)
        put :update, params: { id: course.to_param, course: valid_attributes }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        course = create(:course)
        put :update, params: { id: course.to_param, course: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested course' do
      course = create(:course)
      expect do
        delete :destroy, params: { id: course.to_param }
      end.to change(Course, :count).by(-1)
    end

    it 'redirects to the courses list' do
      course = create(:course)
      delete :destroy, params: { id: course.to_param }
      expect(response).to redirect_to(root_path)
    end
  end
end
