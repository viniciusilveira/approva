require 'rails_helper'

RSpec.describe InstitutionsController, type: :controller do
  let(:valid_attributes) { attributes_for(:institution) }

  let(:invalid_attributes) { attributes_for(:institution, grade: nil) }

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}
      expect(response).to be_success
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      institution = create(:institution)
      get :edit, params: { id: institution.to_param }
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Institution' do
        expect do
          post :create, params: { institution: valid_attributes }
        end.to change(Institution, :count).by(1)
      end

      it 'redirects to the  root path after create institution' do
        post :create, params: { institution: valid_attributes }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { institution: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        attributes_for(:institution)
      end

      it 'updates the requested institution' do
        institution = create(:institution)
        put :update, params: { id: institution.to_param, institution: new_attributes }
        institution.reload
        expect(institution.name).to eq(new_attributes[:name])
        expect(institution.grade.to_s).to eq(new_attributes[:grade])
      end

      it 'redirects to the institution' do
        institution = create(:institution)
        put :update, params: { id: institution.to_param, institution: valid_attributes }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        institution = create(:institution)
        put :update, params: { id: institution.to_param, institution: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested institution' do
      institution = create(:institution)
      expect do
        delete :destroy, params: { id: institution.to_param }
      end.to change(Institution, :count).by(-1)
    end

    it 'redirects to the institutions list' do
      institution = create(:institution)
      delete :destroy, params: { id: institution.to_param }
      expect(response).to redirect_to(root_path)
    end
  end
end
