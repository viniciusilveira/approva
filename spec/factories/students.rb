FactoryGirl.define do
  factory :student do
    name { Faker::StarWars.character }
    grade { Faker::Number.decimal(2) }
    course
  end
end
