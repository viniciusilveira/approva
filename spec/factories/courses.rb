FactoryGirl.define do
  factory :course do
    name { Faker::StarWars.planet }
    grade { Faker::Number.decimal(2) }
    institution
  end
end
