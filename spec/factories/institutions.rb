FactoryGirl.define do
  factory :institution do
    name { Faker::RickAndMorty.character }
    grade { Faker::Number.decimal(2) }
  end
end
