require 'rails_helper'

RSpec.describe Institution, type: :model do
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :grade }

  it { is_expected.to respond_to(:name) }
  it { is_expected.to respond_to(:grade) }
end
