require 'rails_helper'

RSpec.describe Student, type: :model do
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :grade }
  it { is_expected.to validate_presence_of :course }

  it { is_expected.to respond_to(:name) }
  it { is_expected.to respond_to(:grade) }
  it { is_expected.to respond_to(:course) }
  it { is_expected.to respond_to(:course_id) }
  it { is_expected.to respond_to(:institution) }
end
