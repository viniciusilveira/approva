FROM ruby

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y nodejs --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get install -y postgresql-client --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get install -y qt5-default qttools5-dev-tools qt5-qmake libqt5webkit5-dev --no-install-recommends && rm -rf /var/lib/apt/lists/*


COPY Gemfile /usr/src/app/
COPY Gemfile.lock /usr/src/app

RUN bundle install
RUN gem install foreman
RUN gem install rubocop

COPY . /usr/src/app

EXPOSE 5000
