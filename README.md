[ ![Codeship Status for viniciusilveira/approva](https://app.codeship.com/projects/01108840-58e7-0135-e5b0-46d534d134a5/status?branch=master)](https://app.codeship.com/projects/236479)

## APPROVA

Basic plataform to monitore universities Enad Rank.

[DEMO](https://approva-vinicius.herokuapp.com/)

## Instalation

First time clone this repo:

```
$ git clone https://viniciusilveira@bitbucket.org/viniciusilveira/approva.git
```

For better experience and more facility install is recommended to user docker:

### Docker install(Ubuntu)

#### docker-engine

In terminal execute:

```
# wget -qO- https://get.docker.com/ | sh
```

After finish run hello-world container to verify instalation is OK:

```
docker container run hello-world
```

output from this command is:

```
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://cloud.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/engine/userguide/
```

#### docker-compose

In terminal execute:

```
# pip install docker-compose
```

#### docker-machine

In terminal execute:

```
# curl -L https://github.com/docker/machine/releases/download/v0.12.1/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine &&
chmod +x /tmp/docker-machine &&
sudo cp /tmp/docker-machine /usr/local/bin/docker-machine
```

*For others distros or SOs follow documentation(https://www.docker.com/)*

### Run Application

Copy .env.example file to .env:

```
$ cp .env.example .env
```

Build docker containers, create databases and run migrations:
In the application folder execute:
```
$ docker-compose build
$ docker-compose run approva rails db:create
$ docker-compose run approva rails db:migrate
$ docker-compose run approva rails db:test:prepare
```

and run application:

```
$ docker-compose up
```

Access in web navigator the url: http://localhost:8080


### Install without Docker

- Install Rails with [rbenv](https://github.com/rbenv/rbenv) or [rvm](https://github.com/rvm/rvm)
- Install postgresql 9.5+

Copy .env.example file to .env:

```
$ cp .env.example .env
```

And set APPROVA_DATABASE_USER and APPROVA_DATABASE_PASSWORD  variables with your database settings:

```
# .env
APPROVA_DATABASE_USER=postgres
APPROVA_DATABASE_PASSWORD=password
```

After install execute follow commands:

```
$ gem install bundler
$ bundle install
$ rails db:create
$ rails db:migrate
$ rails db:test:prepare
```

and start application:

```
$ rails server
```
Access in web navigator the url: http://localhost:3000

### Tests

For the run tests with Docker run this:

```
$ docker-compose run approva rspec
```
or
```
$ docker-compose run approvra rspec /path/to/test file
```

without docker:

```
$ bundle exec rspec
```
or
```
$ bundle exec rspec /path/to/test file
```
