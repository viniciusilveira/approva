class Institution < ApplicationRecord
  validates :name, :grade, presence: true

  has_many :courses
end
