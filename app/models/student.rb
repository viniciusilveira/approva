class Student < ApplicationRecord
  validates :name, :grade, :course, presence: true

  belongs_to :course

  def institution
    course.institution
  end
end
