class Course < ApplicationRecord
  validates :name, :grade, :institution, presence: true

  belongs_to :institution
  has_many :students
end
