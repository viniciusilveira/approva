class ListController < ApplicationController
  def new
    @list = ListService.new(list_params).data
  end

  def list_params
    params.permit(:institution_name, :institution_grade, :course_name, :course_grade, :student_grade)
  end
end
