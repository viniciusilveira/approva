class ListService
  attr_accessor :institution_name, :institution_grade, :course_name, :course_grade, :student_grade

  def initialize(params = {})
    self.institution_name = params[:institution_name]
    self.institution_grade = params[:institution_grade]
    self.course_name = params[:course_name]
    self.course_grade = params[:course_grade]
    self.student_grade = params[:student_grade]
  end

  def data
    institutions = Institution.left_outer_joins(courses: [:students])
                              .select('institutions.id AS institution_id, institutions.name AS institution_name, institutions.grade AS institution_grade,
               courses.id AS course_id, courses.name AS course_name, courses.grade AS course_grade,
               students.id AS student_id, students.name AS student_name, students.grade AS student_grade')
                              .order('institutions.grade DESC')
    institutions = institutions.where('institutions.name LIKE ?', "%#{institution_name}%") unless institution_name.blank?
    institutions = institutions.where('institutions.grade = ?', institution_grade) unless institution_grade.blank?
    institutions = institutions.where('courses.name LIKE ?', "%#{course_name}%") unless course_name.blank?
    institutions = institutions.where('courses.grade = ?', course_grade) unless course_grade.blank?
    institutions = institutions.where('students.grade = ?', student_grade) unless student_grade.blank?

    institutions
  end
end
