class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :name
      t.decimal :grade
      t.integer :course_id
      t.timestamps
    end

    add_foreign_key :students, :courses
  end
end
