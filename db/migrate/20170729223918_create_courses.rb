class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :name
      t.decimal :grade
      t.integer :institution_id
      t.timestamps
    end

    add_foreign_key :courses, :institutions
  end
end
